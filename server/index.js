var server_port = process.env.PORT || 5000;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var express = require('express'),
app 		= require('express')(),
server 		= app.listen(server_port, function () {
  console.log( "Listening on " +  ", port " + server_port )
}),
path 		= require('path'),
bodyParser 	= require('body-parser'),
publicPath 	= '/../public/kasihub'

app.use(express.static(path.resolve(__dirname + '/../bower_components')))

app.use(express.static(path.resolve(__dirname + publicPath)))

app.get('/', function (req, res) {
	res.sendFile(path.resolve(__dirname, publicPath, 'index.html'))
})

