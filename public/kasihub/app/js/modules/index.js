$(document).ready(function() {
  $('.time').append(time);
  createEvent(event1.img, 'event1');
  createEvent(event2.img, 'event2');
  createEvent(event3.img, 'event3');
  createEvent(event4.img, 'event4');
  createEvent(event5.img, 'event5');
  setFirstImage();
  updateBehind();
});

$('.bottomPopup').on('click', '.goingButton', function() {
  toggleGoing();
});
  
$('.bottomMenu').on('click', '.browse', function() {
  toggleAttendList();
});

$('.attendList').on('click', '.back', function() {
  toggleAttendList();
});

var event1 = {
  img: 'assets/images/pp.jpg',
  title: 'Web Design',
  loc: 'London, UK',
  date: '12th Dec 2017', 
  attending: 'Attend'
},  event2 = {
  img: 'https://i.pinimg.com/736x/a0/ae/c2/a0aec245c3441a74b6b468069fbd500b--woman-portrait-dry-hair.jpg',
  title: 'Fashion',
  loc: 'Dublin, Ireland',
  date: '17th Dec 2017', 
  attending: 'Attend'
},  event3 = {
  img: 'https://hushphotography.co.uk/wp-content/uploads/2017/08/hush-sitemap.jpg',
  title: 'Photography',
  loc: 'New York, USA',
  date: '19th Dec 2017', 
  attending: 'Attend'
},  event4 = {
  img: 'https://fthmb.tqn.com/CIyO228QhhwvdHFjjtIBfyGq2V8=/768x0/filters:no_upscale()/about/Engineer-58eb9fdf3df78c51626eacf8.jpg',
  title: 'Robotics',
  loc: 'Los Angeles, USA',
  date: '22th Dec 2017', 
  attending: 'Attending'
},  event5 = {
  img: 'http://24.media.tumblr.com/0a875bd986a521c3ed0c9716ddcc2da3/tumblr_n1oexzlyuZ1qef46ho1_500.gif',
  title: 'Comedy',
  loc: 'Manchester, UK',
  date: '21th Dec 2017', 
  attending: 'Attend'
};

var eventsTotal = 0;
function createEvent(img, id) {
  $('ul.images').append('<li class="img" id="'+id+'" style="background-image: url(\''+img+'\');"><div class="attend"></div></li>');
  eventsTotal++;
}

function setFirstImage() {
  $('ul li.img').last().addClass('sel');
  var name = $('.sel').attr('id')
  selectedEventDetails(name);
}

$('ul').on('click', '.p1', function() {
  $('ul li.img.sel').addClass('m').removeClass('sel');
  $(this).addClass('sel').removeClass('p1');
  updateSel();
  updateBehind();
});

$('ul').on('click', '.sel', function() {
  $(this).toggleClass('full');
  $('.bottomPopup').toggleClass('full');
  $('.topBar').toggleClass('full');
  updateBehind();
});

$('.navArrow').click(function() {
  $('ul li.img.sel').next().addClass('sel').removeClass('m p1 p2');
  $('ul li.img.sel').prev().removeClass('sel');
  updateSel();
  updateBehind();
});

function updateSel() {
  var name = $('.sel').attr('id')
  selectedEventDetails(name);
}

function updateBehind() {
  $('ul .img.sel').prevAll().addClass('p3').removeClass('m, sel, p1, p2');
  $('ul .img.sel').prev().prev().addClass('p2').removeClass('m, sel, p1, p3');
  $('ul .img.sel').prev().addClass('p1').removeClass('m, sel, p2, p3');
  if ($('ul li.img').last().hasClass('sel')) {
    $('.navArrow').removeClass('show')
  } else {
    $('.navArrow').addClass('show')
  }
};

function selectedEventDetails(event) {
  var topDetails  = '<div class="eventName">'+window[event].title+'</div>';
      topDetails += '<div class="eventPlace">'+window[event].loc+'</div>';
      topDetails += '<div class="eventDate">'+window[event].date+'</div>';
  $('.topDetails').html(topDetails);
  var attending = window[event].attending;
  
  if (attending == 'Attending') {
    $('#'+event+' .attend').addClass('Attending').html('<i class="fa fa-check"></i> Attending');
  } else {
    $('#'+event+' .attend').removeClass('Attending').html('');
  }
  var bottomPopup  = '<div class="goingButton '+attending+'">'+attending+'</div>';
      bottomPopup += '<div class="eventName">'+window[event].title+'</div>';
      bottomPopup += '<div class="eventPlace">'+window[event].loc+'</div>';
      bottomPopup += '<div class="eventDate">'+window[event].date+'</div>';
      bottomPopup += '<div class="cal"><i class="fa fa-plus-circle"></i> Add to my calendar</div>';
  $('.bottomPopup').html(bottomPopup);
}

function toggleGoing() {
  var id = $('.sel').attr('id');
  if ((window[id].attending) == 'Attend') {
    window[id].attending = 'Attending';
    selectedEventDetails(id);
  } else {
    window[id].attending = 'Attend';
    selectedEventDetails(id);
  }
}

function toggleAttendList() {
  $('.attendList ul li').remove();
  for (var i=1; i<=eventsTotal; i++) {
    if (window['event'+i].attending == 'Attending') {
      $('.attendList ul').append('<li>'+window['event'+i].title+' - '+window['event'+i].date+'</li>')
    }
  }
  $('.attendList').toggleClass('show');
}

var dt = new Date();
var getMinutes = dt.getMinutes();
if (dt.getMinutes() <= 9) {
  getMinutes = '0'+getMinutes;
} else {
  getMinutes = getMinutes;
}
var time = dt.getHours() + ":" + getMinutes;