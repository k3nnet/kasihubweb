
app.factory('ProgressDialog', ['$mdDialog', function ($mdDialog) {

    return {
        show: show,
        hide: hide
    }

    function show() {

        $mdDialog.show({
            template: '<md-dialog id="plz_wait" style="box-shadow:none">' +
            '<md-dialog-content layout="row" layout-margin layout-padding layout-align="center center" aria-label="wait">' +
            '<md-progress-circular md-mode="indeterminate" md-diameter="50"></md-progress-circular>' +
            'Loading...' +
            '</md-dialog-content>' +
            '</md-dialog>',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: false,
            escapeToClose: false
        });
    }

    function hide() {
        $mdDialog.cancel()
    }

}]);



app.factory('Auth', ['$firebaseAuth', '$cookieStore', 'ProgressDialog', '$location', '$timeout', 'UserService', function ($firebaseAuth, $cookieStore, ProgressDialog, $mdDialog, $location, $timeout, UserService) {

    var auth = $firebaseAuth();

    this.$cookieStore = $cookieStore;

    var account = {};



    var mDialog = $mdDialog;


    return {
        getAuth: getAuth,
        login: login,
        register: register,
        logout: logout,
        isLoggedIn: isLoggedIn,
        getUserByEmail: getUserByEmail,
        getAccount: getAccount

    }

    function getAccount() {
        return account;
    }






    function getAuth() {
        return auth;
    }

    function getUserByEmail(user_email) {
        //if user is logged in retrieve user details in user database using the user's email
        var ref = firebase.database().ref("users");
        console.log("login")
        return ref.orderByChild("email").equalTo(user_email).once("child_added").then(function (snapshot) {
            console.log(snapshot.val());
            //on successfull retrieval of user details

            key = snapshot.key;




            return {
                account: snapshot.val(),
                id: snapshot.key
            };

            //listen to any value changes for the retrieved vendor details
            /* */



        }).catch(function (err) {
            console.log(err);
            return;
        });
    }



    function login(user) {




        return auth.$signInWithEmailAndPassword(user.email, user.password).then(function (auth) {

            //check if user is in the users database
            console.log("auth")
            console.log(auth.email);
            var ref = firebase.database().ref("users");
            
            
            console.log("login")
            return ref.orderByChild("email").equalTo(auth.email).once("child_added").then(function (snapshot) {
                console.log(snapshot.val());

                var user = snapshot.val();
                console.log(user);

                var vendorRef=firebase.database().ref('Vendors/');
                var id=snapshot.val().id;

               return vendorRef.orderByKey().equalTo(id).once("value").then(function (snapshot) {
                   console.log("=========snapshot=================")

                   var businessAccount;
                   
                    snapshot.forEach(function(data){
                        console.log(data.val())
                        businessAccount=data.val();
                        
                        
                    })

                    if(!snapshot.numChildren()==0){
                        console.log("available")

                        account.user=user;
                        account.id=user.id;
                        account.singedIn=true;
                        $cookieStore.put('account', account);

                        return{
                            signedIn:true
                        }


                        
                    }else{
                       console.log("not available")
                       return{
                           message:"please finish up your regestritation"
                       }
                    }
                      //on successfull retrieval of user details

             
                }).catch(function(error){
                    console.log("=============error======");
                    console.log(error);
                })




            }).catch(function (err) {
                console.log(err);
                return;
            });



        }, function (error) {
            console.log(error)

            return error;


        });
    }

    function register(user) {

        this.$timeout = $timeout;
        ProgressDialog.show()
        var Name = user.name;
        var lastName = user.lastName;
        var username = user.username;
        var email = user.email;
        var address = user.address;
        var mobileNumber = user.mobileNumber;


        // Create a new user
        auth.$createUserWithEmailAndPassword(scope.user.email, scope.user.password)
            .then(function (firebaseUser) {
                console.log("=====================Registering user=====================")
                scope.message = "User created with uid: " + firebaseUser.uid;
                //create a userId for the new user
                var userId = firebase.database().ref('users').push().key;
                console.log(userId);

                var user = {
                    name: Name,
                    surname: lastName,
                    phone: mobileNumber,
                    Address: address,
                    photoUrl: "",
                    email: email,
                    role: "vendor",
                    id: userId
                };


                //save the user in the database
                firebase.database().ref('users').child(userId).set(user);
                console.log("++++++++++++++++user registerd+++++++++++++++++++++++++++++")


                ProgressDialog.hide();

                //go to next page
                $state.go('register');
            }).catch(function (error) {
                scope.error = error;

                console.log(error);

                ProgressDialog.hide()

            });




    }

    function logout() {

        return firebase.auth().signOut().then(function () {
            // Sign-out successful.
            $cookieStore.remove('account');
            $cookieStore.remove('vendor');
            account={};
            console.log("sign-out successful");
            return {
                successful: true

            }
        }).catch(function (error) {
            // An error happened.
            return {
                successful: false,
                error: error

            }
        });


    }

    function isLoggedIn() {



    }

}]);


app.factory('UserService', [function () {

    var account = {};

    return {
        set: set,
        getUser: getUser,
        updateAccount: updateAccount
    };


    function updateAccount(id, account) {

        console.log(id);


        return firebase.database().ref('/Vendors/' + id).update(account).then(function (results) {

            return firebase.database().ref('/Vendors/' + id).once('value').then(function (snapshot) {

                return {
                    account: snapshot.val(),
                    successful: true
                }

            })
        })


    }

    function set(obj) {
        user = obj;
        console.log("user set to " + JSON.stringify(user));

    }



    function getUser(user) {

        var name, email, photoUrl, uid, emailVerified;
        var error = {};
        console.log(user);

        return firebase.database().ref('/users').orderByChild("email").equalTo(user.email).once('value').then(function (snapshot) {

            if (snapshot.val() == null) {
                error.message = "user not found "
                console.log("user not found");
                return error;

            } else if (snapshot.val() != null) {
                var key = "";
                var user = {};
                snapshot.forEach(function (childSnapshot) {

                    key = childSnapshot.key;

                    user = childSnapshot.val();

                });
                console.log(user);
                console.log(key);
                account.user = user
                account.key = key;
                return { user: user, key: key }




            }
        });

    }






}])



app.factory('ProductServices', ['$firebase', '$firebaseArray', function ($firebase, $firebaseArray) {

    var name = "";
    var key;

    var productRef;

    return {
        getMenuProducts: getMenuProducts,
        addCatalogueItem: addCatalogueItem,
        addMenuHeader: addMenuHeader,
        uploadImage: uploadImage,
        getCatalogueProducts: getCatalogueProducts,
        updateCatalogueProduct: updateCatalogueProduct,
        deleteCatalogueProduct: deleteCatalogueProduct,
        deleteListItem: deleteListItem,
        addListItem: addListItem,
        updateListItem: updateListItem


    }



    function addListItem(menuItem) {

        console.log("menu Item");
        console.log(menuItem);
        console.log(this.name);
        console.log(this.key);
        var name = this.name;
        var key = this.key;


        var itemRef = firebase.database().ref('Menus/' + key + '/' + name).orderByChild('title').equalTo(menuItem.header.title);
        console.log("menu item added " + menuItem.name + " " + menuItem.description + " " + menuItem.price);
        var findItem = $firebaseArray(itemRef);


        var item = {
            name: menuItem.name,
            description: menuItem.description,
            price: menuItem.price
        }


        if (menuItem.imageUrl) {

            item.imageUrl = menuItem.imageUrl;

        } else {
            item.imageUrl = '';
        }



        var findItem = $firebaseArray(itemRef);



        return findItem.$loaded().then(function (x) {

            console.log("find item");
            console.log(findItem);

            var childIndex = findItem[0].$id;

            if (findItem[0].childList == null) {
                var itemIndex = 0;
            }
            else {
                var itemIndex = findItem[0].childList.length;

            }

            item.headerIndex = childIndex;
            item.childIndex = itemIndex;



            return firebase.database().ref('Menus/' + key + '/' + name).child(childIndex + '/childList').child(itemIndex).set(item).then(function (result) {

                //count to keep number of menu itmes
                firebase.database().ref('Menus/' + key + '/' + name).child(childIndex + '/count').set(itemIndex + 1);



                var index = findItem[0].count;

                console.log("done");
                console.log(result);
                return {
                    successful: true
                }

            });






        })
            .catch(function (error) {
                console.log("Error:", error);
                return {
                    successful: false
                }

            });


    }




    function updateListItem(newItem) {
        console.log(newItem);

        console.log("id: " + this.key + " name: " + this.name);
        var refString = firebase.database().ref('Menus/' + this.key + '/' + this.name + '/' + newItem.headerIndex + '/childList/');

        var items = $firebaseArray(refString);
        console.log(items);

        return items.$loaded().then(function (items) {


            var item = items.$getRecord(newItem.childIndex);
            console.log(item);
            item.name = newItem.name;
            item.description = newItem.description;
            item.price = newItem.price;
            item.headerIndex = newItem.headerIndex;
            item.childIndex = newItem.childIndex;
            item.imageUrl = newItem.imageUrl;
            items.$save(item);
            return {
                successful: true
            }

        });

    }



    function getMenuProducts(key, name) {


        console.log("id key: " + key + " name: " + name);

        this.key = key;
        this.name = name;


        console.log(name)
        listRef = firebase.database().ref('Menus/' + key + '/' + name);

        var menu = $firebaseArray(listRef)

        console.log(JSON.stringify(menu));

        return menu;


    }

    function addMenuHeader(menuHeader) {

        console.log(menuHeader);
        console.log(this.name);
        console.log(this.key)

        var name = this.name;
        var id = this.key;

        console.log("menu header id: " + id + " menu header name: " + name);
        var headers = this.getMenuProducts(id, name);



        //handle the callback
        return headers.$loaded()
            .then(function (x) {
                x === headers; // true
                console.log("here is the list " + JSON.stringify(x))

                var index = headers.length;
                console.log("headers size:" + index)
                console.log(id + " " + name);

                //add item to  menu database
                return firebase.database().ref('Menus/' + id + '/' + name).child(index).set(menuHeader).then(function (results) {
                    console.log(results);
                    return firebase.database().ref('Menus/' + id + '/' + name).once('value').then(function (snapshot) {
                        console.log(snapshot.val());
                        return {
                            successful: true,
                            products: snapshot.val()
                        }


                    })




                });


            })
            .catch(function (error) {
                console.log("Error:", error);
            });






    }


    function uploadImage(key, name, fileItem) {
        //upload image to firebase storage

        var uploadTask = firebase.storage().ref('KasiHub/Catalogue/' + key + '/' + name).child(fileItem.name).put(fileItem);

        return uploadTask;



    }

    function addCatalogueItem(item, name) {
        console.log(item);
        var product = {

            productName: item.productName,
            imageUrl: item.imageUrl,
            productDescription: item.productDescription,
            price: item.price

        }

        console.log("vendorkey: " + key + " vendorName: " + name);
        console.log(product)

        productRef = firebase.database().ref('Catalogue/' + key + '/' + name);

        var id = productRef.push(product).key
        product.id = id;

        //add item to  catalogue database
        return firebase.database().ref('Catalogue/' + key + '/' + name + '/' + id).set(product).then(function (results) {
            console.log(results);
            return firebase.database().ref('Catalogue/' + key + '/' + name).once('value').then(function (snapshot) {
                console.log(snapshot.val());
                return {
                    successful: true,
                    products: snapshot.val()
                }


            })




        });
    }

    function getCatalogueProducts(id, name) {
        var type = ""

        key = id;

        console.log("id: " + key + " name: " + name);

        console.log(name)
        catalogueRef = firebase.database().ref('Catalogue/' + key + '/' + name);

        products = $firebaseArray(catalogueRef)

        console.log(JSON.stringify(products.key));

        return products;







        /*  return firebase.database().ref('Catalogue/' + id + '/' + name).once('value').then(function (snapshot) {
                  console.log(snapshot.val());
                  return {
                      successful: true,
                      products: snapshot.val()
                  }
  
  
              })*/





    }

    function updateCatalogueProduct(product, name) {

        console.log(product.id);
        console.log(name);

        return firebase.database().ref('Catalogue/' + key + '/' + name + '/' + product.id).update(product).then(function (results) {

            return firebase.database().ref('Catalogue/' + key + '/' + name).once('value').then(function (snapshot) {

                return {
                    user: snapshot.val(),
                    successful: true
                }

            })
        })
    }

    function deleteListItem(header, product, index) {


        console.log(this.name);
        console.log(name);
        console.log("id: " + key + " name: " + name);




        var headerIndex = product.headerIndex;
        var childIndex = product.childIndex;

        var hashKey = "$$hashKey";
        // /-KqxF8PLcEdkTmLJtJCT/Blue Mansion/1/childList/0


        console.log("childIndex before: " + childIndex)
        var refString = 'Menus/' + this.key + '/' + this.name + '/' + headerIndex + '/childList/' + childIndex;
        console.log(refString);
        firebase.database().ref(refString).remove();
        console.log("childindex after: " + childIndex);

        //count to keep number of menu itmes
        return firebase.database().ref('Menus/' + this.key + '/' + this.name).child(headerIndex + '/count').set(childIndex).then(function (result) {
            console.log("result " + result);
            return
        })

    }

    function deleteCatalogueProduct(product, name) {

        return firebase.database().ref('Catalogue/' + key + '/' + name + "/" + product.id).remove().then(function (result) {
            console.log(result);

            return firebase.database().ref('Catalogue/' + key + '/' + name).once('value').then(function (snapshot) {
                console.log(snapshot.val());
                return {
                    successful: true,
                    products: snapshot.val()
                }


            });

        })

    }
}])


app.factory('VendorService', ['$mdDialog', 'ProductServices', 'ProgressDialog', '$firebase', '$firebaseArray', '$firebaseObject', '$cookieStore', function ($mdDialog, $firebaseArray, $cookieStore, $firebaseAuth, ProgressDialog, $firebase, $location, $timeout) {

    var vendor = {};
    var item = {};
    var headers = [];
    var catalogue = [];
    var vendors = [];
    var vendorsRef = firebase.database().ref('Vendors');
    var catalogueRef;
    var error = {};

    this.$firebaseArray = $firebaseArray;


    var headerIndex = '';
    var vendorIndex = '';
    var name = '';
    var id = '';


    return {

        setVendor: setVendor,
        getVendor: getVendor,
        setItem: setItem,
        getItem: getItem,
        getVendorProducts: getVendorProducts,
        addMenuItem: addMenuItem,
        nextPage: nextPage,
        getVendors: getVendors,
        addCatalogueItem: addCatalogueItem,
        showDialog: showDialog,
        cancelDialog: cancelDialog,
        editDialog: editDialog,
        editProduct: editProduct,
        addVendor: addVendor
    }


    function getVendor(key) {
        //get the vendor associated with the user details
        vendorRef = firebase.database().ref('/Vendors/' + key);
        return vendorRef.once('value').then(function (snapshot) {
            console.log("vendor: " + JSON.stringify(snapshot.val()));

            vendor = snapshot.val();

            console.log("vendor set: " + JSON.stringify(snapshot.val()));

            return vendor;


        });
    }




    function addVendor(account, key) {

        console.log(account);

        var businessName = account.vendor.name;
        var businessPhone = account.vendor.phone;
        var businessLocation = account.vendor.location;
        var businessDescription = account.vendor.description;
        var businessCategory = account.vendor.category;
        businessCategory.toLowerCase();
        var businessWhatsapp = account.vendor.whatsapp;
        var businessEmail = account.vendor.email;
        if (account.vendor.regID != null) {
            var businessRegID = account.vendor.regID;
        } else {
            var businessRegID = "";
        }

        var businessView = account.vendor.view;


        console.log(key);
        var vendorDetails = {
            name: businessName,
            phone: businessPhone,
            Address: businessLocation,
            photoUrl: "",
            lattitude: -24.2994597,
            longitude: 29.5363884,
            location: businessLocation,
            description: businessDescription,
            category: businessCategory,
            whatsapp: businessWhatsapp,
            email: businessEmail,
            regID: businessRegID,
            mode: businessView,
            view: businessView

        };



        //save the user in the database
        firebase.database().ref('users').child(key).set(account.user);


        return firebase.database().ref('Vendors/' + key).set(vendorDetails).then(function (results) {

            return firebase.database().ref('Vendors/' + key).once('value').then(function (snapshot) {

                return {
                    user: snapshot.val(),
                    successful: true
                }

            })
        })





    }


    function editProduct(newProduct) {
        var ref = catalogueRef;

        var catalogue = $firebaseArray(ref);
        catalogue.$add(newProduct).then(function (ref) {
            var id = ref.key();
            console.log("added record with id " + id);
            catalogue.$indexFor(id); // returns location in the array
        });


    }


    function setItem(item) {
        this.item = item;
    }

    function getItem() {
        return vendor;
    }



    function editDialog(item) {
        $mdDialog.show({
            templateUrl: 'assets/directives/productView/editDialog.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            fullscreen: false,
            escapeToClose: false,
            controller: DialogController,
            controllerAs: 'dialogCtrl'
        });

        function DialogController($scope, FileUploader, $firebaseArray) {

            $scope.newItem = {};
            $scope.newItem.name = item.productName;
            $scope.newItem.showImage = true;
            $scope.newItem.description = item.productDescription;
            $scope.newItem.price = item.price;
            $scope.newItem.imageUrl = item.imageUrl;
            $scope.newItem.id = item.$id;



            console.log(item);

            var uploader = $scope.uploader = new FileUploader({
                url: 'upload.php'
            });



            // FILTERS

            uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    $scope.file = item;


                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });

            // CALLBACKS

            uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                console.info('onWhenAddingFileFailed', item, filter, options);

            };
            uploader.onAfterAddingFile = function (fileItem) {

                console.info('onAfterAddingFile', fileItem);
                console.log(fileItem.file.name);
                $scope.fileName = fileItem.file.name;
                $scope.showImage = false;


            };
            uploader.onAfterAddingAll = function (addedFileItems) {
                console.info('onAfterAddingAll', addedFileItems);
                var files = $scope.files = addedFileItems;
            };
            uploader.onBeforeUploadItem = function (item) {


                console.info('onBeforeUploadItem', item);
            };
            uploader.onProgressItem = function (fileItem, progress) {
                console.info('onProgressItem', fileItem, progress);
            };
            uploader.onProgressAll = function (progress) {
                console.info('onProgressAll', progress);
            };
            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                console.info('onSuccessItem', fileItem, response, status, headers);
            };
            uploader.onErrorItem = function (fileItem, response, status, headers) {
                console.info('onErrorItem', fileItem, response, status, headers);
            };
            uploader.onCancelItem = function (fileItem, response, status, headers) {
                console.info('onCancelItem', fileItem, response, status, headers);
            };
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                console.info('onCompleteItem', fileItem, response, status, headers);
            };
            uploader.onCompleteAll = function () {
                console.info('onCompleteAll');
            };

            console.info('uploader', uploader);

            $scope.updateItem = function (newItem) {
                console.log(newItem)

                var ref = catalogueRef;
                var items = $firebaseArray(ref);
                console.log(items);
                items.$loaded().then(function (items) {

                    var item = items.$getRecord(newItem.id);
                    console.log(item);
                    item.productName = newItem.name;
                    item.productDescription = newItem.description;
                    item.price = newItem.price;
                    items.$save(item);
                    cancelDialog();
                });





            }
        }

    }

    function showDialog() {
        $mdDialog.show({
            template: '<md-dialog id="plz_wait" style="box-shadow:none">' +
            '<md-dialog-content layout="row" layout-margin layout-padding layout-align="center center" aria-label="wait">' +
            '<md-progress-circular md-mode="indeterminate" md-diameter="50"></md-progress-circular>' +
            'Loading...' +
            '</md-dialog-content>' +
            '</md-dialog>',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: false,
            escapeToClose: false
        });
    }

    function cancelDialog() {
        $mdDialog.cancel()
    }





    function addCatalogueItem(item, fileItem) {


        this.showDialog()
        console.log(item);
        console.log(JSON.stringify(fileItem))

        var ProductName = item.productName;
        var ProductDescription = item.productDescription;
        var ProductPrice = item.price;







        var uploadTask = firebase.storage().ref('KasiHub/Catalogue/' + id + '/' + name).child(fileItem.name).put(fileItem);

        uploadTask.on('state_changed', function (snapshot) {

            var progress = (snapshot.bytesTransferred / snapshot.totalBytes)

            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:
                    console.log("upload paused");
                    break;
                case firebase.storage.TaskState.RUNNING:
                    console.log("upload in progress");
                    break;


            }
        }, function (error) {
            console.log("unsuccsefful upload" + error.message);
        }, function () {
            var downloadURL = uploadTask.snapshot.downloadURL;
            var product = {

                productName: item.productName,
                imageUrl: downloadURL,
                productDescription: item.productDescription,
                price: "R" + item.price

            }

            console.log(JSON.stringify(product))

            catalogue.push(product);
            cancelDialog();




        });



        // console.log(+" " + this.$scope.productDescription + " " + this.$scope.productPrice + " " + this.$scope.uploader.isHTML);


    }
    function getVendorById(key) {

        return firebase.database().ref('/Vendors/' + key).on('value').then(function (snapshot) {

            console.log(snapshot.val());
            if (snapshot.val() == null) {


                console.log("vendor does not exist");
                error.message = "vendor does not exist";
                return error;
            } else if (snapshot.val() != null) {

                return snapshot.val();
            }
        });
    }

    function nextPage() {
        vendorRef = firebase.database().ref('Menus/' + id + '/' + name).startAt(5);

        headers = $firebaseArray(vendorRef);

        return headers;

    }

    function setVendor(v) {
        vendor = v;
    }

    function getCatProduct(product) {

        var id = product.$id;
        console.log(id);


    }

    function addMenuItem(menuItem) {

        console.log(menuItem.header.title);
        var itemRef = vendorRef.orderByChild('title').equalTo(menuItem.header.title);
        console.log("menu item added " + menuItem.name + " " + menuItem.description + " " + menuItem.price);
        //console.log("selected header "+menuItem.header.title+" on index "+index);
        this.showDialog();
        var item = {
            name: menuItem.name,
            description: menuItem.description,
            price: menuItem.price
        }


        var menu = $firebaseArray(itemRef);
        menu.$loaded()
            .then(function (x) {

                var childIndex = menu[0].$id;

                if (menu[0].childList == null) {
                    var itemIndex = 0;
                }
                else {
                    var itemIndex = menu[0].childList.length;

                }



                var dbRef = vendorRef.child(childIndex + '/childList');
                dbRef.child(itemIndex).set(item);
                vendorRef.child(childIndex + '/count').set(itemIndex + 1);



                var index = menu[0].count;
                cancelDialog()
                console.log();



            })
            .catch(function (error) {
                console.log("Error:", error);
                cancelDialog()
            });


    }


    function getVendors() {




        vendors = $firebaseArray(vendorsRef);
        vendors.$watch(function (event) {
            console.log(event);
            console.log(vendors.length);
            vendorIndex = vendors.length;
        });
        console.log(JSON.stringify(vendors.key));

        return vendors;


    }


    function getVendorProducts(key, name) {



        console.log("id: " + key + " name: " + name);

        console.log(name)
        vendorRef = firebase.database().ref('Menus/' + key + '/' + name);

        headers = $firebaseArray(vendorRef)
        headers.$watch(function (event) {
            console.log(event);
            console.log(headers.length);
            headerIndex = headers.length;
        });
        console.log(JSON.stringify(headers.key));

        return headers;


    }





}]);

app.factory('DashboardService', ['$firebase', '$firebaseArray', '$firebaseObject', function ($firebaseArray, $firebaseAuth, $firebase) {

}])

app.factory('EventService', ['$firebase', function ($firebase) {

    var event = {};

    return {
        getEvents: getEvents,
        createEvent: createEvent
    }


    function getEvents() {



        return firebase.database().ref('Events').on('value').then(function (snapshot) {
            console.log(snapshot.val())

            return snapshot.val();
        });






    }

    function createEvent(ev) {

        var event = {

            eventName: ev.name,
            imageUrl: ev.imageUrl,
            eventDescription: ev.description,
            date: ev.date,
            createdBy: ev.createdBy,
            fbUrl: "",
            venue: ev.venue

        }

        console.log("event" + ev);
        console.log(event)

        var eventRef = firebase.database().ref('Events');

        var id = firebase.database().ref('Events').push(event).key


        return firebase.database().ref('Events' + id).set(product).then(function (results) {

            return firebase.database().ref('Events').once('value').then(function (snapshot) {

                return {
                    user: snapshot.val(),
                    successful: true
                }

            })
        })

    }

}])




