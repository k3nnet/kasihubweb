
var app = angular.module('KasiHub', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.router',
    'firebase',
    'ngCookies',
    'ngMessages',
    'ngAria',
    'ngMaterial',
    'ngMap',
    'angularFileUpload',
    'angular-google-analytics'
]);


app.config(['AnalyticsProvider', function (AnalyticsProvider) {
   // Add configuration code as desired
   AnalyticsProvider.setAccount('UA-117236224-1');  //UU-XXXXXXX-X should be your tracking code
     AnalyticsProvider.trackPages(true);
      AnalyticsProvider.setPageEvent('$stateChangeSuccess');
       AnalyticsProvider.setDomainName('none');

       // Set hybrid mobile application support
  AnalyticsProvider.setHybridMobileSupport(true);

}])




app.run(['$trace', '$transitions', '$cookieStore', 'Auth', 'VendorService','$window','$state','Analytics', function ($trace,$transitions, $cookieStore,$state, Auth, VendorService,$window,Analytics) {

   
    $trace.enable('TRANSITION');
    
     var auth;

 
 $transitions.onBefore({to:'dashboard.**'},function(transition){
      Auth.account=$cookieStore.get('account');
     console.log("=====================Entering dashboard=================");
     auth=transition.injector().get('Auth');
     console.log( Auth.account);
     if(! Auth.account){

         return transition.router.stateService.target("login");
     }

 });

$transitions.onEnter({}, function(transition, state) {
  console.log('Transition #' + transition.$id + ' Entered ' + state.name);
 

 
  console.log((transition));
})


 $transitions.onBefore({to:'login'},function(transition){
      Auth.account=$cookieStore.get('account');

     auth=transition.injector().get('Auth');

     if( Auth.account){
         return transition.router.stateService.target('dashboard.table');
     }

     
 })

 $transitions.onError({},function(transition){
     console.log(transition.error());
 })
    
 
 


  


}]);


app.controller('EventsController', function (EventService) {


    EventService.getEvents().then(function (results) {

        console.log(results);

        $scope.event = results;

    })





})


app.controller('BusinessRegisterController', function ($state, ProgressDialog, $scope, $cookieStore, Auth, VendorService) {

    $scope.business = {};




    var user = $cookieStore.get('user');

    console.log(user);
    var key = user.id;




    if (!(user == null)) {

        $scope.business.email = user.email;
        $scope.business.phone = user.phone;




    }


    $scope.addVendor = function addVendor(business) {
        var account = {
            id: key,
            user: user,
            vendor: business
        }
        $cookieStore.put('account', account);
        ProgressDialog.show();
        console.log(business);
        VendorService.addVendor(account, key).then(function (result) {
            ProgressDialog.hide();
            console.log(result);
            if (result.successful) {
                $state.go('dashboard.table');
            }

        })


    };
})




app.controller('DashboardCtrl', function ($cookieStore, ProductServices, $rootScope, firebase, $http, Auth, $state, FileUploader, $scope, UserService, ProgressDialog, $firebaseArray, VendorService) {

    // ProgressDialog.show();

    var vendorRef = "";
    var catalogueRef = firebase.database().ref('Catalogue/' + key + '/' + name);
    var listRef = firebase.database().ref('Menus/' + key + '/' + name);
    $scope.toggle = true;
    $scope.form = true;
    $scope.edit = false;
    $scope.button = "Add Item";
    $scope.funct = "addProduct";

    $scope.vendor = {};
    $scope.product = {};
    $scope.editProduct = ""
    $scope.products = [];
    $scope.headers = [];

    var headers = [];
    var products = [];
    var workingHours = [];
    var vendor = {};
    var name = "";
    var key = "";
    var imageUrl = "";


    $scope.vendor = $cookieStore.get('vendor');
    console.log(vendor);





    $scope.logout = function () {
        Auth.logout().then(function (result) {
            if (result.successful) {
                $state.go('login');
            } else {
                $scope.error = result.error;
            }
        });
    }









    //====================================Profile========================//

    //edit profile

    $scope.editProfile = function (vendor) {

        console.log('profile');
        console.log(vendor);

        $scope.editVendor = vendor;


    }

    $scope.updateProfile = function (editBusiness) {
        console.log(editBusiness);
    }





    $scope.logout = function () {
        Auth.logout();
    }




    //console.log(Auth.isLoggedIn);


})

app.controller('home', function (firebase, Auth, $state) {
    console.log(Auth.isLoggedIn);

    Auth.onAuthStateChanged(function (user) {
        if (user) {


            $state.go('/dashboard')
        } else {
            // No user is signed in.
        }
    });

})

app.controller('AuthCtrl', function (Auth, $location, $state, $scope, firebase, ProgressDialog, UserService, VendorService) {

    $scope.user = {
        email: '',
        password: ''
    };

    $scope.error = {};



    var showDialog = function () {
        ProgressDialog.show()
    }

    var hideDialog = function () {
        ProgressDialog.hide();
    }


    //login a user
    $scope.login = function () {

        ProgressDialog.show();
        Auth.login($scope.user).then(function (result) {
            ProgressDialog.hide()
            console.log(Auth.getAccount());
            var account = Auth.getAccount();
            if (result.message) {
                $scope.error.message = result.message;
                $state.go('register');
            }
            else if (result.signedIn) {
                $state.go('dashboard.table');
            }
        })






        /*  Auth.getAuth().$signInWithEmailAndPassword($scope.user.email, $scope.user.password).then(function (auth) {
  
              //check if user is in the users database
              console.log("auth")
              console.log(auth.email);
              var ref = firebase.database().ref("users");
              var userId = firebase.auth().currentUser.uid;
              var user = firebase.auth().currentUser;
              console.log(userId);
              firebase.database().ref('/users').orderByChild("email").equalTo(auth.email).once('value').then(function (snapshot) {
                  ProgressDialog.hide();
                  if (snapshot.val() == null) {
  
  
  
  
  
                      $scope.error.message = "please register first before login "
                      console.log("user is not registered");
                  } else if (snapshot.val() != null) {
                      var key = "";
                      var user = {};
                      snapshot.forEach(function (childSnapshot) {
  
                          key = childSnapshot.key;
  
                          user = childSnapshot.val();
  
                      });
                      console.log(user);
                      console.log(key);
  
                      firebase.database().ref('/Vendors/' + key).once('value').then(function (snapshot) {
                          ProgressDialog.hide();
                          console.log(snapshot.val());
                          if (snapshot.val() == null) {
                              UserService.set()
                              $state.go('register');
                              console.log("vendor does not exist");
                          } else if (snapshot.val() != null) {
                              VendorService.setVendor(snapshot.val());
                              $state.go('dashboard.table');
                          }
                      });
  
  
                  }
              });
  
  
  
  
  
  
          }, function (error) {
              console.log(error)
              $scope.error = error;
              ProgressDialog.hide()
          });*/

        // Auth.login($scope.user);
    };

    //register a user

    $scope.register = function () {



    }




});

app.controller('PreviewController', function ($scope, $state) {


    $scope.next = function () {
        console.log("button clicked");

    }

    $scope.onHomePage = function () {
        return ($state.go() === '/' || $state.go() === '#/');
    };



});



app.controller('TableController', function ($window, $cookieStore, ProductServices, $rootScope, firebase, $http, Auth, $state, FileUploader, $scope, UserService, ProgressDialog, $firebaseArray, VendorService, $stateParams) {


    console.log($stateParams.id);
    var id = $stateParams.id;


    var vendorRef = "";
    var catalogueRef = firebase.database().ref('Catalogue/' + key + '/' + name);
    var listRef = firebase.database().ref('Menus/' + key + '/' + name);
    $scope.toggle = true;
    $scope.form = true;
    $scope.edit = false;
    $scope.view = false;
    $scope.button = "Add Item";
    $scope.funct = "addProduct";

    $scope.vendor = {};
    $scope.product = {};
    $scope.editProduct = ""
    $scope.products = [];
    $scope.headers = [];

    var headers = [];
    var products = [];
    var workingHours = [];
    var vendor = {};
    var name = "";
    var key = "";
    var imageUrl = "";


    var uploader = $scope.uploader = new FileUploader();
    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            $scope.file = item;


            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);

    };
    uploader.onAfterAddingFile = function (fileItem) {

        console.info('onAfterAddingFile', fileItem);
        console.log("added file " + fileItem.file.name);

        // var uploadTask=ProductServices.uploadImage(key,name,$scope.file);

        imageUrl = ProductServices.uploadImage(key, name, $scope.file)






        $scope.edit = true;
        $scope.fileName = fileItem.file.name;


    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
        $scope.files = addedFileItems;
    };
    uploader.onBeforeUploadItem = function (item) {


        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.uploader = uploader;


    firebase.auth().onAuthStateChanged(function (user) {
        console.log(user);
        if (user) {


            console.log("on auth state change in dashboard");
            console.log($cookieStore.get('account'));

            var account = $cookieStore.get('account');
            key = account.id;


            VendorService.getVendor(account.id).then(function (result) {


                console.log(result);
                console.log('+====================================')
                result.id = key;
                VendorService.vendor = result;
                console.log(result);
                $cookieStore.put('vendor', result);

                var vendor = $cookieStore.get('vendor');
                name = vendor.name;
                key = vendor.id;
                console.log(VendorService.vendor);
                console.log($cookieStore.get('vendor'));
                console.log(firebase.auth().currentUser);


                //show preferred view depending on vendor view
                if (vendor.view == 'list') {

                    console.log("youre in the list view")  //show listing view
                    console.log($cookieStore.get('vendor').id);

                    $scope.view = false;


                    //get the list of items given the user key and vendor name

                    var headers = ProductServices.getMenuProducts(vendor.id, vendor.name);



                    //handle the callback


                    headers.$loaded()
                        .then(function (x) {

                            x === headers; // true
                            console.log("here is the list " + x.length)
                            if (x.length == 0) {
                                $scope.message = "No items found.please add your product";
                                $scope.toggle = false;
                            }
                            $scope.headers = x;

                        })
                        .catch(function (error) {
                            console.log("Error:", error);
                            ProgressDialog.hide();

                        });
                }

                else if (vendor.view == 'catalogue') {
                    console.log("catalogue view in place thats whassapp")  //catalogue view

                    $scope.view = true;



                    var catalogue = ProductServices.getCatalogueProducts(vendor.id, vendor.name);



                    catalogue.$loaded()
                        .then(function (x) {

                            if (x.length == 0) {
                                $scope.toggle = false;
                            }



                            console.log("here is the list " + JSON.stringify(x))
                            $scope.products = x;
                        })
                        .catch(function (error) {
                            console.log("Error:", error);
                            ProgressDialog.hide();

                        });








                }










            })













        } else {

            //if user not signed in close dialog and redirect to login page
            console.log("user not signed in ")
            $state.go('login', {}, { reload: true });
            // No user is signed in.

        }

    });














    //products = $cookieStore.get('products');
    //console.log(products);
    // $scope.products = products;

    $scope.toggles = function () {

        if ($scope.toggle == true) {
            console.log("toggle : " + $scope.toggle);
            $scope.toggle = false;
        } else {
            console.log("toggle : " + $scope.toggle);

            $scope.toggle = true;
        }
    }







    //====================================Table============================================//



    //add menuHeader by taking a menuHeader object as a parameter
    $scope.addMenuHeader = function (menuHeader) {
        var header = {
            title: $scope.menuHeader.title,
            count: 0,
            initiallyExpanded: true,
            childList: null
        }

        ProgressDialog.show();
        ProductServices.addMenuHeader(header).then(function (result) {
            console.log(result);
            ProgressDialog.hide();

            if (result.successful) {

                $scope.menuHeader.title = "";

            }

        });




    }







    //add list item
    $scope.addListItem = function (menuItem) {

        ProgressDialog.show();

        console.log(menuItem);


        var file = $scope.file;


        if (file) {



            ProductServices.uploadImage(key, name, file).then(function (result) {

                console.log(result.downloadURL);
                menuItem.imageUrl = result.downloadURL;
                console.log(menuItem);

                ProductServices.addListItem(menuItem).then(function (result) {
                    ProgressDialog.hide();
                    console.log('item added');
                    console.log(result);
                    if (result.successful) {


                        $scope.menuItem = "";
                        $scope.uploader.clearQueue();
                        uploader.clearQueue();


                    }
                })


                var menu = ProductServices.getMenuProducts(key, name);






            })






        } else {


            $scope.menuItem = {};

            ProductServices.addListItem(menuItem).then(function (result) {
                ProgressDialog.hide();
                console.log('item added');
                console.log(result);
                if (result.successful) {

                    $scope.menuItem = {};

                }
            })



        }
    }




    //add catalogue item
    $scope.addProduct = function (item) {
        ProgressDialog.show();


        ProductServices.uploadImage(key, name, $scope.file).then(function (result) {
            console.log("new image url from funciton")
            console.log(result.downloadURL);
            item.imageUrl = result.downloadURL;





            ProductServices.addCatalogueItem(item, name).then(function (result) {
                ProgressDialog.hide();
                console.log(result);
                $scope.products = result.products;
                $scope.product = {};
                $scope.toggle = false;
                $scope.uploader.clearQueue();
                $scope.uploader.cancelAll();
                uploader.cancelAll();
                uploader.clearQueue();

            });


        })



    }




    $scope.addItem = function (product) {
        VendorService.addCatalogueItem(product, $scope.file);
    }

    //edit list item
    $scope.editListItem = function (header, product, index) {
        console.log(header);
        console.log(product);

        $window.scrollTo(0, angular.element('#editForm').offsetTop);

        $scope.editProduct = product;
        $scope.editProduct.headerIndex = header.$id;
        $scope.editProduct.header = header;

        $scope.editProduct.childIndex = index;
        $scope.toggle = false;
        $scope.form = false;

    }

    $scope.editItem = function (item) {

        console.log(item);
        $scope.form = false;
        $window.scrollTo(0, angular.element('#editForm').offsetTop);
        $scope.toggle = false;
        $scope.button = "Update Item"
        $scope.funct = "updateProduct";

        if ($scope.header) {
            $scope.editProduct.header = $scope.header.title;
        }

        $scope.editProduct = item;



    }


    //updates catalogue item
    $scope.updateProduct = function (newItem) {
        ProgressDialog.show();
        console.log("imageUrl in update " + imageUrl);
        console.log(newItem)

        if (imageUrl === "") {
            var productImage = newItem.imageUrl
            console.log("here empty")
            var productName = newItem.productName;
            var productDescription = newItem.productDescription;
            var price = newItem.price

            console.log(productName + " : " + productDescription + " : " + price + ": " + productImage)

            ProductServices.updateCatalogueProduct(newItem, name).then(function (result) {
                ProgressDialog.hide();

                console.log(result);

            })


        } else {

            ProductServices.uploadImage(key, name, $scope.file).then(function (result) {
                console.log(result.downloadURL);


                newItem.imageUrl = result.downloadURL;


                var productName = newItem.productName;
                var productDescription = newItem.productDescription;
                var price = newItem.price

                console.log(productName + " : " + productDescription + " : " + price + ": " + productImage)

                ProductServices.updateCatalogueProduct(newItem, name).then(function (result) {
                    ProgressDialog.hide();

                    $scope.editProduct = {};

                    $scope.form = true;
                    $scope.uploader.clearQueue();
                    $scope.uploader.cancelAll();
                    uploader.cancelAll();
                    uploader.clearQueue();

                    console.log(result);

                })


            })


        }



        /*   var ref = catalogueRef;
           var items = $firebaseArray(ref);
           console.log(items);
           items.$loaded().then(function (items) {
     
               var item = items.$getRecord($scope.editProduct.$id);
               console.log(item);
               item.productName = productName;
               item.productDescription = productDescription;
               item.price = price;
               item.imageUrl = productImage;
               items.$save(item);
               $scope.editProduct = "";
               ProgressDialog.hide();
     
           });
           $scope.form = false;
           uploader.clearQueue();*/
    }

    //updates list item
    $scope.updateListItem = function (newItem) {

        ProgressDialog.show();
        var file = $scope.file;

        if (file) {

            console.log(key + ": name=" + name)

            ProductServices.uploadImage(key, name, file).then(function (result) {

                newItem.imageUrl = result.downloadURL;
                ProductServices.updateListItem(newItem).then(function (result) {
                    ProgressDialog.hide();
                    console.log(result);
                    $scope.toggle = true;
                    $scope.form = true;
                    $scope.edit = false;
                    uploader.clearQueue();
                    $scope.uploader.clearQueue();
                    uploader = $scope.uploader = new FileUploader();
                })

            })
        }
        else {

            ProductServices.updateListItem(newItem).then(function (result) {
                ProgressDialog.hide();
                console.log(result);
                $scope.toggle = true;
                $scope.form = true;
                $scope.edit = false;
            })

        }


    }






    // remove item from listview
    $scope.removeListItem = function (header, product, index) {


        console.log(product);
        ProgressDialog.show();

        ProductServices.deleteListItem(header, product, index).then(function (result) {
            ProgressDialog.hide();
            console.log(result)
        });








    }



    //remove item from catalogue
    $scope.removeCatalogueItem = function (product) {
        ProgressDialog.show();

        ProductServices.deleteCatalogueProduct(product, name).then(function (results) {
            console.log(results);
            $scope.products = results.products;
            ProgressDialog.hide();
        })






    }


    //get image url
    var getImageUrl = function () {





        var fileItem = $scope.file;

        var uploadTask = firebase.storage().ref('KasiHub/Catalogue/' + key + '/' + name).child(fileItem.name).put(fileItem);

        return uploadTask.on('state_changed', function (snapshot) {

            var progressPercentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:
                    console.log("upload paused");
                    break;
                case firebase.storage.TaskState.RUNNING:
                    console.log("upload in progress");
                    break;


            }
        }, function (error) {
            ProgressDialog.hide();
            $scope.error;
            console.log("unsuccsefful upload" + error.message);
        }, function () {
            uploader.clearQueue();
            imageUrl = uploadTask.snapshot.downloadURL;

            console.log("image url is : " + imageUrl)

            return imageUrl







        });
    };


    //get all items in catalogue
    var getCatalogueProducts = function (key, name) {



        console.log("id: " + key + " name: " + name);

        console.log(name)
        catalogueRef = firebase.database().ref('Catalogue/' + key + '/' + name);

        products = $firebaseArray(catalogueRef)

        console.log(JSON.stringify(products.key));

        return products;


    }


    //get all items in list
    var getMenuProducts = function (key, name) {



        console.log("id: " + key + " name: " + name);

        console.log(name)
        listRef = firebase.database().ref('Menus/' + key + '/' + name);

        headers = $firebaseArray(listRef)
        headers.$watch(function (event) {
            console.log(event);
            console.log(headers.length);
            headerIndex = headers.length;
        });
        console.log(JSON.stringify(headers.key));

        return headers;


    }






});


app.controller('getListed', function ($scope, $state) {


    $scope.next = function () {
        console.log("button clicked");

    }

    $scope.onHomePage = function () {
        return ($state.go() === '/' || $state.go() === '#/');
    };



});

app.controller('BusinessProfileController', function ($scope, ProductServices, UserService, $firebase, VendorService, ProgressDialog, $cookieStore, Auth, FileUploader) {


    var key;
    var name;
    $scope.logo = {};
    var imageUrl = ""

    var uploader = $scope.uploader = new FileUploader();
    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            $scope.file = item;


            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);

    };
    uploader.onAfterAddingFile = function (fileItem) {

        console.info('onAfterAddingFile', fileItem);
        console.log("added file " + fileItem.file.name);

        // var uploadTask=ProductServices.uploadImage(key,name,$scope.file);

        imageUrl = ProductServices.uploadImage(key, name, $scope.file)






        $scope.edit = true;
        $scope.fileName = fileItem.file.name;


    };




    var account = $cookieStore.get('account');


    console.log(account);
    ProgressDialog.show();

    VendorService.getVendor(account.id).then(function (result) {
        ProgressDialog.hide();
        console.log(result);
        console.log(result.photoUrl);

        if (result.photoUrl === "") {
            console.log("logo not set")
            $scope.logo.url = 'app/img/new_logo.png'
        } else {
            console.log("logo set:" + result.photoUrl)
            $scope.logo.url = result.photoUrl;

        }

        $scope.editBusiness = result;


    })






    $scope.updateProfile = function (editBusiness) {
        var profile = $scope.editBusiness
        console.log($scope.file);
        ProgressDialog.show();

        if (imageUrl === "") {



            UserService.updateAccount(account.id, profile).then(function (result) {
                ProgressDialog.hide();
                console.log(result)



            })





        } else {

            ProductServices.uploadImage(account.id, profile.name, $scope.file).then(function (result) {
                console.log(result.downloadURL);


                profile.photoUrl = result.downloadURL;

                UserService.updateAccount(account.id, profile).then(function (result) {
                    ProgressDialog.hide();
                    console.log(result)
                    uploader.clearQueue();
                    $scope.uploader = uploader;

                    $scope.editBusiness = result.account;


                })






            })


        }



    }

})

app.controller('MapController', function (NgMap) {

    NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);
    })
})


app.controller('WorkingHoursController', function ($scope, $cookieStore, ProgressDialog, $firebaseArray) {
    var account = $cookieStore.get('account');
    var vendor = $cookieStore.get('vendor');

    console.log(vendor);






    //get working hours
    var getWorkingHours = function (key, name) {



        console.log("id: " + key + " name: " + name);

        console.log(name)
        workingHoursRef = firebase.database().ref('workingHours/' + key + '/' + name);


        workingHours = $firebaseArray(workingHoursRef)
        console.log(workingHours)


        return workingHours;


    }


    //get working hours
    var hours = getWorkingHours(account.key, vendor.name);
    //handle the callback
    ProgressDialog.show();
    hours.$loaded().then(function (x) {
        ProgressDialog.hide();
        x === hours; // true
        console.log("hours " + JSON.stringify(x))
        $scope.workingHours = x;
    })
        .catch(function (error) {
            console.log("Error:", error);
        });





    //==================================Working Hours==========================================//


    //add working hours
    $scope.addWorkingHours = function (workingHour) {

        console.log(workingHour);

        var workingHour = $scope.workingHour;

        var openingTime = workingHour.openingTime + "";
        var closingTime = workingHour.closingTime + "";


        console.log(openingTime);
        openingTime = openingTime.substring(15, 21);
        closingTime = closingTime.substring(15, 21);



        var workingHourItem = openingTime + " - " + closingTime + " -" + $scope.workingHour.startDay + "-" + $scope.workingHour.endDay;

        var workingHoursRef = firebase.database().ref('workingHours/' + account.id + '/' + vendor.name);

        var workingHours = $firebaseArray(workingHoursRef)
        var index = workingHours.length;




        workingHoursRef.child(index).set(workingHourItem).then(function (results) {
            console.log(results);
            $scope.workingHour = "";
            workingHours.$loaded().then(function (x) {
                ProgressDialog.hide();
                x === hours; // true
                console.log("hours " + JSON.stringify(x))
                $scope.workingHours = x;
            })
                .catch(function (error) {
                    console.log("Error:", error);
                });




        })


    }



    //edit working hours

    $scope.editWorkingHours = function (item) {
        console.log(item);

        $window.scrollTo(0, angular.element('#editForm').offsetTop);


        $scope.editProduct = item;



    }

    //update working hours
    $scope.updateWorkingHours = function (newItem) {

        console.log(newItem);

        console.log("id: " + key + " name: " + name);

        var refString = firebase.database().ref('WorkingHours/' + account.id + '/' + account.user.name);

        var items = $firebaseArray(refString);
        console.log(items);
        items.$loaded().then(function (items) {

            var item = items.$getRecord(childIndex);
            console.log(item);

            items.$save(item);

        });
    }



})


