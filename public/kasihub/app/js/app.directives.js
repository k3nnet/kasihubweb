///////////////////////////////////////////////////
////////////////// Directives ////////////////// //
////////////////////////////////////////////////////

app.directive('preview', function ($location) {
  return {
    restrict: 'E',
    scope: {
    },
    templateUrl: 'app/templates/directives/sections/preview.html',
    link: function (scope) {


    }
  };

});

app.directive('progressDialog', function () {

  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/circularProgress.html',
    scope: {
      progress: '@progress'
    },
    link: function (scope, elem, attr) {
      console.log(scope.progress);

    }

  }

})

app.directive('features', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/sections/features.html',
    scope: {

    },
    link: function (scope) {

    }
  }
});

app.directive('signUp', function (Auth,$cookieStore, $state, firebase, ProgressDialog, UserService, VendorService) {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/templates/directives/sections/signup.html',
    link: function (scope, elem, attr) {

      scope.register = function () {

        if (scope.signupForm.$valid) {
          ProgressDialog.show()
          var Name = scope.user.name;
          var lastName = scope.user.lastname;
          var username = scope.user.username;
          var email = scope.user.email;
          var address = scope.user.address;
          var mobileNumber = scope.user.mobileNumber;
          scope.message = null;
          scope.error = null;

          // Create a new user account
          Auth.getAuth().$createUserWithEmailAndPassword(scope.user.email, scope.user.password)
            .then(function (firebaseUser) {

                    console.log("=====================Registering user=====================")
                scope.message = "User created with uid: " + firebaseUser.uid;
                //create a userId for the new user
                var userId = firebase.database().ref('users').push().key;
                console.log(userId);

                var user = {
                    name: Name,
                    surname: lastName,
                    phone: mobileNumber,
                    Address: address,
                    photoUrl: "",
                    email: email,
                    role: "vendor",
                    id: userId
                };


                //save the user in the database
                firebase.database().ref('users').child(userId).set(user);
            console.log("++++++++++++++++user registerd+++++++++++++++++++++++++++++")
              $cookieStore.put('user',user);

              ProgressDialog.hide();

              //go to next page
              $state.go('register');
            }).catch(function (error) {
              scope.error = error;
              // UserService.set(user);
              console.log(error);

              if(error.message==="The email address is already in use by another account."){
                  scope.error.message="The email address is already in use by another account.please login "
                  scope.inUse=true;
              }

              ProgressDialog.hide()

              //go to next page
              // $state.go('register');
            });
        } else {

          console.log(signupForm.$error);
        }

      }
    }

  }
});

app.directive('download', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/sections/download.html'
  }
});

app.directive('socialFooter', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/sections/footer.html'
  }
})

app.directive('catalogueView', function () {
  return {
    restrict: 'E',
    scope: {
      addItem: '&'
    },
    controller:'TableController',
    templateUrl: 'app/templates/dashboard/productView/catalogue/catalogue.html',
    link: function (scope, el, VendorService) {

      scope.$on("close dialog", function (product) {

        el.find('div').eq(0).modal('hide');

      });

    }
  };

});
app.directive('listView', function () {
  return {
    restrict: 'E',
    scope: {
      addItem: '&'
    },
    controller:'TableController',
    templateUrl: 'app/templates/dashboard/productView/listView/list.html',
    link: function (scope, el, VendorService) {



    }
  };

});
app.directive('editListItem', function () {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/templates/dashboard/productView/listview/editProduct.html',
    link: function (scope, el) {


      scope.edit = function (editedProduct) {

        scope.updateProduct(editedProduct);
        el.find('div').eq(0).modal('hide');
        scope.editedProduct = '';
      };

    }
  };

});
app.directive('ngThumb', ['$window', function ($window) {
  var helper = {
    support: !!($window.FileReader && $window.CanvasRenderingContext2D),
    isFile: function (item) {
      return angular.isObject(item) && item instanceof $window.File;
    },
    isImage: function (file) {
      var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  };

  return {
    restrict: 'A',
    template: '<canvas/>',
    link: function (scope, element, attributes) {
      if (!helper.support) return;

      var params = scope.$eval(attributes.ngThumb);

      if (!helper.isFile(params.file)) return;
      if (!helper.isImage(params.file)) return;

      var canvas = element.find('canvas');
      var reader = new FileReader();

      reader.onload = onLoadFile;
      reader.readAsDataURL(params.file);

      function onLoadFile(event) {
        var img = new Image();
        img.onload = onLoadImage;
        img.src = event.target.result;
      }

      function onLoadImage() {
        var width = params.width || this.width / this.height * params.height;
        var height = params.height || this.height / this.width * params.width;
        canvas.attr({ width: width, height: height });
        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
      }
    }
  };
}]);

app.directive('editItem', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/dashboard/productView/catalogue/editProduct.html',
    link: function (scope, el) {


      scope.edit = function (editedProduct) {

        scope.updateProduct(editedProduct);
        el.find('div').eq(0).modal('hide');
        scope.editedProduct = '';
      };

    }
  };

});
app.directive('addListItem', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/dashboard/productView/listview/addProduct.html',
    link: function (scope, el) {

      //   scope.manualItem.price="place gikse";

      scope.add = function (product) {

        scope.addItem(product);
        el.find('div').eq(0).modal('hide');
        scope.product = '';
      };

    }
  };

});


app.directive('addItem', function () {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/dashboard/productView/catalogue/addProduct.html',
    link: function (scope, el) {

      //   scope.manualItem.price="place gikse";

      scope.add = function (product) {

        scope.addItem(product);
        el.find('div').eq(0).modal('hide');
        scope.product = '';
      };

    }
  };

});

app.directive('navBar', function () {

  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/navbar.html'
  }
})

app.directive('events',function(){

  return{
    restrict:'E',
    templateUrl:'app/templates/directives/sections/events.html'
  }
})




/////////////////////////////////////////////////////
////////////////// Controllers ////////////////// //
////////////////////////////////////////////////////


class PreviewController {
  constructor($scope) {
    'ngInject'

    $scope.something = "just to see if it works";
  }
}