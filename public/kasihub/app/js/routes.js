///////////////////////////////////////////////////
//////////////////  Routes  ////////////////// //
//////////////////////////////////////////////////

app.config(function ($stateProvider, $urlRouterProvider) {

 

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'app/templates/home.html'
  }).state('login', {
    url: '/login',
    templateUrl: 'app/templates/login.html'

  }).state('events', {
    url: '/events',
    templateUrl: 'app/templates/events.html',
    controller: 'EventsController'

  }).state('board', {
    url: '/board',
    templateUrl: 'app/templates/kasiBoard.html'

  }).state('register', {
    url: '/register',
    templateUrl: 'app/templates/forms/businessDetails.html',
    controller: 'BusinessRegisterController'
  }).state('dashboard', {
    url: '/dashboard',
    abstract: true,
    controller: 'DashboardCtrl',
    templateUrl: 'app/templates/dashboard/dashboard.html'
  }).state('dashboard.profile', {
    url: '/profile',
    templateUrl: 'templates/user.html',
    controller: 'BusinessProfileController'
  }).state('dashboard.table', {
    url: '/tables',
    templateUrl: 'app/templates/dashboard/table.html',
    controller: 'TableController'
  }).state('dashboard.workingHours', {
    url: '/workingHours',
    templateUrl: 'app/templates/forms/workingHours.html',
    controller: 'WorkingHoursController'
  }).state('dashboard.map', {
    url: '/address',
    templateUrl: 'app/templates/dashboard/map.html',
    controller: 'MapController'
  });

   $urlRouterProvider.otherwise('/home');

});