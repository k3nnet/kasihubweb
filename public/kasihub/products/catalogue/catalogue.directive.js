export function CatalogueListingDirective() {
    'ngInject';

    let directive = {
        restrict: 'E',
        templateUrl: 'app/products/catalogue/catalogueListing.html',
        controller: CatalogueListController,
        controllerAs: 'catalogueList',
        bindToController: true
    };

    return directive;
}

class CatalogueListController {
    constructor($scope, firebase, vendorService, FileUploader) {
        'ngInject';

        this.$scope = $scope;
        this.firebase = firebase;
        this.vendorService = vendorService;
        this.$scope.button = "Add product";
        var storage = firebase.storage();
        var uploader = this.$scope.uploader = new FileUploader({
            url: 'upload.php'
        });



        // FILTERS

        uploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                $scope.file = item;


                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);

        };
        uploader.onAfterAddingFile = function (fileItem) {

            console.info('onAfterAddingFile', fileItem);
            console.log(fileItem.file.name);
            $scope.fileName = fileItem.file.name;


        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
            var files = $scope.files = addedFileItems;
        };
        uploader.onBeforeUploadItem = function (item) {


            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        //listen for catalogue view broadcast
        this.$scope.$on('catalogueView', function () {
            // $scope.language = someService.language;
            console.log("broadcast recieved from catalogueView");

            $scope.menuItems = []

            var vendor = vendorService.getVendor();

            var products = vendorService.getCatalogueProducts(vendor);

            products.$loaded()
                .then(function (x) {
                    x === products; // true
                    console.log("here is the list " + JSON.stringify(x))
                    $scope.products = x;
                })
                .catch(function (error) {
                    console.log("Error:", error);
                })
        });








    }


    editProduct(item) {



        this.vendorService.setItem(item);
        this.vendorService.editDialog(item)
       
    }


    showDialog() {
        this.vendorService.showDialog();
    }


    addCatalogueItem(item, file) {
        console.log("registering " + JSON.stringify(item));
        console.log(this.$scope.file)


        this.vendorService.addCatalogueItem(item, this.$scope.file)
        this.$scope.product.name = "";
        this.$scope.product.description = "";
        this.$scope.product.price = "";
        this.$scope.uploader.isHTML5 = false;

    }
}

